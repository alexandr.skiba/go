const path = require('path');
const os = require('os');
const fs = require('fs');

// Node.js doesn't have a built-in multipart/form-data parsing library.
// Instead, we can use the 'busboy' library from NPM to parse these requests.
const Busboy = require('busboy');
const FormData = require('form-data');
const axios = require('axios');

const token = process.env.TOKEN;
const channel = process.env.CHANNEL;

exports.uploadFile = (req, res) => {
  if (req.method === 'OPTIONS') {
    // Send response to OPTIONS requests
    res.set('Access-Control-Allow-Methods', 'GET');
    res.set('Access-Control-Allow-Headers', 'Content-Type');
    res.set('Access-Control-Max-Age', '3600');
    res.status(204).send('');
  } else {
    if (req.method !== 'POST') {
      // Return a "method not allowed" error
      return res.status(405).end();
    }
    res.set('Access-Control-Allow-Origin', '*');

    const busboy = new Busboy({ headers: req.headers });
    const tmpdir = os.tmpdir();

    // This object will accumulate all the fields, keyed by their name
    const fields = {};

    // This object will accumulate all the uploaded files, keyed by their name.
    const uploads = {};

    // This code will process each non-file field in the form.
    busboy.on('field', (fieldname, val) => {
      // TODO(developer): Process submitted field values here
      console.log(`Processed field ${fieldname}: ${val}.`);
      fields[fieldname] = val;
    });

    const fileWrites = [];

    // This code will process each file uploaded.
    busboy.on('file', (fieldname, file, filename) => {
      // Note: os.tmpdir() points to an in-memory file system on GCF
      // Thus, any files in it must fit in the instance's memory.
      console.log(`Processed file ${filename}`);
      const filepath = path.join(tmpdir, filename);
      uploads[fieldname] = { filepath, filename };

      const writeStream = fs.createWriteStream(filepath);
      file.pipe(writeStream);

      // File was processed by Busboy; wait for it to be written to disk.
      const promise = new Promise((resolve, reject) => {
        file.on('end', () => {
          writeStream.end();
        });
        writeStream.on('finish', resolve);
        writeStream.on('error', reject);
      });
      fileWrites.push(promise);
    });

    // Triggered once all uploaded files are processed by Busboy.
    // We still need to wait for the disk writes (saves) to complete.
    busboy.on('finish', () => {
      Promise.all(fileWrites)
        .then(() => {
          // TODO(developer): Process saved files here
          const form = new FormData();
          form.append('token', token);
          form.append('channels', channel);

          let acc = '';
          for (const name in fields) {
            acc += `>*${name}*: ${fields[name]}\n`;
          }
          form.append('initial_comment', acc);
          for (const name in uploads) {
            const { filepath, filename } = uploads[name];
            form.append('file', fs.createReadStream(filepath));
          }
          if (!Object.keys(uploads).length) {
            return axios.post(
              'https://slack.com/api/chat.postMessage',
              {
                channel,
                text: acc,
              },
              { headers: { Authorization: `Bearer ${token}`, 'Content-Type': 'application/json' } }
            );
          }
          return axios.post('https://slack.com/api/files.upload', form, {
            headers: form.getHeaders(),
          });
        })
        .then((slackRes) => {
          console.log(slackRes.data);
          res.send(slackRes.data);
        })
        .catch((e) => {
          res.status(500).send(e.message);
        });
    });

    busboy.end(req.rawBody);
  }
};